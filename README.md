# Chat App

## Install Dependencies
- npm install

## Start Application
- npm run start

## Use Application
- access localhost:3000 in different tabs
- enter your unique display name
- enter room name (users must enter the same room name to chat with each other)
- type a message in the input box and click the send button
- click the send location button to send your current location